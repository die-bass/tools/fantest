#define NUMBER_FANS 3

typedef struct {
	int pin;
	uint8_t speed;
	String name;
} Fan;

Fan fans[NUMBER_FANS] = {
  {44, 0, "inlet"}, //OCR4B
	{45, 0, "amp1"}, //OCR5B
	{6,  0, "amp2"}, //OCR5C
};

void initPwm() {
	Serial.print("Initializing PWM...");
	// modify PWM regeister to change PWM freq
	// set PWM freq to 31kHz (31372.55 Hz) on ...
	TCCR3B = TCCR3B & B11111000 | B00000001; // ... Pins D2  D3  D5
	TCCR4B = TCCR4B & B11111000 | B00000001; // ... Pins D6  D7  D8
	TCCR5B = TCCR5B & B11111000 | B00000001; // ... Pins D44 D45 D46

	for (int fan_idx = 0; fan_idx < NUMBER_FANS; fan_idx++)
		pinMode(fans[fan_idx].pin, OUTPUT);

	Serial.println("done");
}

void setup()
{
	Serial.begin(9600);
	Serial.println("Test Starting...");

	initPwm();
}

void loop()
{
	Serial.println("--- First setting PWM to 0.0 on all fans ---");
	for (int fan_idx = 0; fan_idx < NUMBER_FANS; fan_idx++) {
		Serial.print("Fan ");
		Serial.print(fans[fan_idx].name);
		Serial.print("(PIN D");
		Serial.print(fans[fan_idx].pin);
		Serial.print("): ");
		Serial.println(0);
		analogWrite(fans[fan_idx].pin, 0);
	}
  delay(10000);

	Serial.println("--- Now turning up the fan PWM ---");
	for (int speed = 10; speed <= 100; speed += 10) {
	  for (int fan_idx = 0; fan_idx < NUMBER_FANS; fan_idx++) {
			Serial.print("Fan ");
			Serial.print(fans[fan_idx].name);
			Serial.print("(PIN D");
			Serial.print(fans[fan_idx].pin);
			Serial.print("): ");
			Serial.println(speed);
			analogWrite(fans[fan_idx].pin, (int)(speed*255/100));
			delay(5000);
		}
	}

	Serial.println("--- Now setting PWM back to 0.0 on all fans ---");
	for (int fan_idx = 0; fan_idx < NUMBER_FANS; fan_idx++) {
		Serial.print("Fan ");
		Serial.print(fans[fan_idx].name);
		Serial.print("(PIN D");
		Serial.print(fans[fan_idx].pin);
		Serial.print("): ");
		Serial.println(0);
		analogWrite(fans[fan_idx].pin, 0);
		delay(20000);
	}
}
